import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';

/*
  Generated class for the HistoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HistoryProvider {

  tracks:Array<any> = [];
  artists:Array<any> = [];
  playlists:Array<any> = [];
  albums:Array<any> = [];

  constructor(public http: HttpClient,
              private nativeStorage: NativeStorage) {
    console.log('Hello HistoryProvider Provider');
  }

  

}
