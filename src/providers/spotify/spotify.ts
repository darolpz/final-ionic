import { Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { HTTP } from '@ionic-native/http';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
/*
  Generated class for the SpotifyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpotifyProvider {

  urlToken = "https://accounts.spotify.com/api/token";
  authorization = "YmY1YzQwN2YwYTA3NDFmZThiN2MyNGFlNjFiNzU3ZmU6YzcxMjZkZmIxZDI0NGQ5M2JhMDJmNDM2Y2VhNzViNTQ=";
  urlSearch = "https://api.spotify.com/v1/search?q=";
  urlSearchArist = "https://api.spotify.com/v1/artists/";
  urlSearchTrack = "https://api.spotify.com/v1/tracks/";
  urlSearchAlbum = "https://api.spotify.com/v1/albums/";
  urlSearchPlaylist = "https://api.spotify.com/v1/users/";
  urlToken2 = 'https://accounts.spotify.com/authorize/?client_id=bf5c407f0a0741fe8b7c24ae61b757fe&response_type=token&redirect_uri=finalSpotify%3A%2F%2Fcallback&&scope=streaming+user-read-birthdate+user-read-email+user-modify-playback-state+user-read-private&show_dialog=true';
  actualToken: any = {
    token: null,
    time: null
  };
  isAndroid: boolean = false;


  constructor(public http: HTTP,
    private _http: HttpClient,
    private platform: Platform,
    private nativeStorage: NativeStorage
  ) {
    this.isAndroid = this.platform.is('android');
  }

  getToken() {
    if (this.actualToken.time == null || this.actualToken.time <= new Date().valueOf()) {
      let headerToken = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
        .append("Authorization", "Basic " + this.authorization);
      let paramToken = new URLSearchParams();
      paramToken.set('grant_type', "client_credentials");
      this._http.post(this.urlToken, paramToken.toString(), { headers: headerToken })
        .subscribe((response: any) => {
          this.actualToken = {
            token: "Bearer " + response.access_token,
            time: new Date().valueOf() + 3600000
          }
        }, error => {
          console.log(<any>error);
        });
    }

  }

  getToken2() {
    return this._http.get(this.urlToken2);
  }

  searchObs(query: string) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(this.urlSearch + query + '&type=artist,track,playlist,album&limit=5', { headers: headerSearch });
  }

  nextPage(url: string) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(url, { headers: headerSearch });
  }

  getArtist(id) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(this.urlSearchArist + id, { headers: headerSearch });
  }
  getTrack(id) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(this.urlSearchTrack + id, { headers: headerSearch });
  }
  getAlbum(id) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(this.urlSearchAlbum + id, { headers: headerSearch });
  }
  getPlaylist(id) {
    let headerSearch = new HttpHeaders().set("Content-Type", "application/x-www-form-urlencoded")
      .append("Authorization", this.actualToken.token);
    return this._http.get(this.urlSearchPlaylist + id.owner.id + '/playlists/' + id.id, { headers: headerSearch });
  }

  playTrack(track: any, flag: boolean) {
    if (!flag) {
      functions.playUri(track.uri);
      track.flag = true;
    } else {
      functions.toogle();
      track.flag = !track.flag;
    }
  }

  getLocalStorage() {
    let promise = new Promise((resolve, reject) => {
      let promises: Array<any> = [];
      /* this.nativeStorage.getItem('albums')
      this.nativeStorage.getItem('tracks')
        .then(r => console.log('TRACKS: ' + JSON.stringify(r)))
        .catch(error => console.log('ERROR: ' + error));
      this.nativeStorage.getItem('playlists')
      this.nativeStorage.getItem('artists') */
      promises.push(this.nativeStorage.getItem('artists'));
      promises.push(this.nativeStorage.getItem('tracks'));
      promises.push(this.nativeStorage.getItem('albums'));
      promises.push(this.nativeStorage.getItem('playlists'));
      

      Promise.all(promises).then(responseArray => {
        resolve(responseArray);
      })
        .catch(catchArray => {
          console.log('Error: ' + catchArray);
        });
    });
    return promise;



  }

  storeAlbum(album: any) {
    let albums = [];
    this.nativeStorage.getItem('albums')
      .then(data => {
        albums = data;
        for (let i = 0; i < albums.length; i++) {
          if (albums[i].id == album.id) {
            albums.splice(i, 1);
          }
        }
        albums.push(album);
        if (albums.length > 5) {
          albums.shift();
        }
        this.nativeStorage.setItem('albums', albums)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });

      }, error => {
        albums.push(album);
        this.nativeStorage.setItem('albums', albums)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });
      })
  }

  storeArtist(artist: any) {
    let artists = [];
    this.nativeStorage.getItem('artists')
      .then(data => {
        artists = data;
        for (let i = 0; i < artists.length; i++) {
          if (artists[i].id == artist.id) {
            artists.splice(i, 1);
          }
        }
        artists.push(artist);
        if (artists.length > 5) {
          artists.shift();
        }
        console.log('2: ' + artists.length);
        this.nativeStorage.setItem('artists', artists)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });

      }, error => {
        artists.push(artist);
        this.nativeStorage.setItem('artists', artists)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });
      })
  }

  storeTrack(track: any) {
    let tracks = [];
    this.nativeStorage.getItem('tracks')
      .then(data => {
        tracks = data;
        for (let i = 0; i < tracks.length; i++) {
          if (tracks[i].id == track.id) {
            tracks.splice(i, 1);
          }
        }
        tracks.push(track);
        if (tracks.length > 5) {
          tracks.shift();
        }
        this.nativeStorage.setItem('tracks', tracks)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });

      }, error => {
        tracks.push(track);
        this.nativeStorage.setItem('tracks', tracks)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });
      })
  }

  storePlaylist(playlist: any) {
    let playlists = [];
    this.nativeStorage.getItem('playlists')
      .then(data => {
        playlists = data;
        for (let i = 0; i < playlists.length; i++) {
          if (playlists[i].id == playlist.id) {
            playlists.splice(i, 1);
          }
        }
        playlists.push(playlist);
        if (playlists.length > 5) {
          playlists.shift();
        }
        this.nativeStorage.setItem('playlists', playlists)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });

      }, error => {
        playlists.push(playlist);
        this.nativeStorage.setItem('playlists', playlists)
          .then(
            () => { },
            error => {
              console.log('error' + JSON.stringify(error));
            });
      })
  }

}
