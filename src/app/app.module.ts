import { PlayListPageModule } from './../pages/play-list/play-list.module';
import { SearchPageModule } from './../pages/search/search.module';

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SpotifyProvider } from '../providers/spotify/spotify';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { TrackPageModule } from '../pages/track/track.module';

import { AlbumPageModule } from '../pages/album/album.module';
import { ArtistPageModule } from '../pages/artist/artist.module';
import { HistoryProvider } from '../providers/history/history';
import { BackgroundMode } from '@ionic-native/background-mode';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    SearchPageModule,
    ArtistPageModule,
    TrackPageModule,
    AlbumPageModule,
    PlayListPageModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SpotifyProvider,
    NativeStorage,
    HistoryProvider,
    BackgroundMode
  ]
})
export class AppModule { }
