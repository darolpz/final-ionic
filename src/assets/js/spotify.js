var player;


const play = ({
  spotify_uri,
  playerInstance: {
    _options: {
      getOAuthToken,
      id
    }
  }
}) => {
  getOAuthToken(access_token => {
    fetch(`https://api.spotify.com/v1/me/player/play?device_id=${id}`, {
      method: 'PUT',
      body: JSON.stringify({ uris: [spotify_uri] }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${access_token}`
      },
    });
  });
};

window.onSpotifyWebPlaybackSDKReady = () => {
  const token = 'BQCEo3uCmRqPF0E0G7h9F375OhxOw_DySDfOZyzWpzOZuekeFsTU3fQ_TXe_s3Izi9IPfaWL1v9iGu1YGW4tI4kzt9vTuElYXTluNqSr2Zp4-8vuVSynoqYpfL_o1Hhs7n_B9ATGYko8glvoxbbrIvn8zpZsPshOKaL4kHpnANIF5EwVLa-X0Nj4GA';


  var urlToken = 'https://accounts.spotify.com/authorize/?client_id=bf5c407f0a0741fe8b7c24ae61b757fe&response_type=token&redirect_uri=finalSpotify%3A%2F%2Fcallback&&scope=streaming+user-read-birthdate+user-read-email+user-modify-playback-state+user-read-private&show_dialog=true';

    player = new Spotify.Player({
      name: 'Web Playback SDK Quick Start Player',
      getOAuthToken: cb => { cb(token); }
    });

    // Error handling
    player.addListener('initialization_error', ({ message }) => { console.error(message); });
    player.addListener('authentication_error', ({ message }) => { console.error(message); });
    player.addListener('account_error', ({ message }) => { console.error(message); });
    player.addListener('playback_error', ({ message }) => { console.error(message); });

    // Playback status updates
    player.addListener('player_state_changed', state => { console.log(state); });

    // Ready
    player.addListener('ready', ({ device_id }) => {
      console.log('Ready with Device ID', device_id);
    });

    // Not Ready
    player.addListener('not_ready', ({ device_id }) => {
      console.log('Device ID has gone offline', device_id);
    });

    // Connect to the player!
    player.connect();
  };

  var functions = {
    playUri: function (uri) {
      play({
        playerInstance: player,
        spotify_uri: uri
      });
    },
    toogle: function () {
      player.togglePlay().then(() => {
      });
    },
    init:function(token){
        
    }
  }
