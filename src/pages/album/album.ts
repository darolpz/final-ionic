import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';

/**
 * Generated class for the AlbumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-album',
  templateUrl: 'album.html',
})
export class AlbumPage {
  album: any = null;
  id: string;
  tracks: Array<any>;
  track_number: number = 0;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public spotifyProvider: SpotifyProvider) {
    this.id = navParams.get('album');
  }

  ionViewDidEnter() {
    this.spotifyProvider.getAlbum(this.id).subscribe(
      (response: any) => {
        this.album = response;
        this.tracks = response.tracks.items;
        this.spotifyProvider.storeAlbum(this.album);
      }, error => {
        console.log(<any>error);
      });

  }

  playTrack(track: any) {
    this.spotifyProvider.playTrack(track, this.track_number == track.track_number);
    this.track_number = track.track_number;
  }
}
