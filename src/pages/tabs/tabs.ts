import { Platform } from 'ionic-angular';
import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SearchPage;
  isAndroid: boolean = false;
  
  constructor(public platform: Platform) {
    this.isAndroid = platform.is('android');
  }
}
