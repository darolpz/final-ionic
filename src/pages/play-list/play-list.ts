import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';

/**
 * Generated class for the PlayListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-play-list',
  templateUrl: 'play-list.html',
})
export class PlayListPage {
  id;
  playlist;
  tracks;
  track_number:number = -1;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private spotifyProvider: SpotifyProvider) {
    this.id = navParams.get('playlist');
  }

  ionViewDidEnter() {
    this.spotifyProvider.getPlaylist(this.id).subscribe(
      (response: any) => {
        this.playlist = response;
        this.tracks = response.tracks.items;
        this.spotifyProvider.storePlaylist(this.playlist);
      }, error => {
        console.log(<any>error);
      });
  }

  playTrack(track:any,index:number){
    this.spotifyProvider.playTrack(track,this.track_number == index);
    this.track_number = index;
  }

}
