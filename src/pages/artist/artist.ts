import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';

/**
 * Generated class for the ArtistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-artist',
  templateUrl: 'artist.html',
})
export class ArtistPage {

  artist;
  id;
  anon = "https://image.freepik.com/iconos-gratis/la-imagen-del-usuario-con-el-fondo-negro_318-34564.jpg";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private spotifyProvider: SpotifyProvider) {
      this.id = navParams.get('artist');
  }

  ionViewDidEnter() {
    this.spotifyProvider.getArtist(this.id).subscribe(
      (response: any) => {
        this.artist = response;
        this.spotifyProvider.storeArtist(this.artist);
      }, error => {
        console.log(<any>error);
      });
  }

}
