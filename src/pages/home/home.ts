import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';

declare var cordova: any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  artists: Array<any> = [];
  albums: Array<any> = [];
  playlists: Array<any> = [];
  tracks: Array<any> = [];
  track_number = -1;
  anon = "https://image.freepik.com/iconos-gratis/la-imagen-del-usuario-con-el-fondo-negro_318-34564.jpg";
  constructor(public navCtrl: NavController,
              private spotifyProvider: SpotifyProvider,
              private platform:Platform) {

  }

  ionViewDidEnter() {
    this.track_number = -1;
    this.spotifyProvider.getLocalStorage()
    .then((data:any) => {
      this.artists = data[0];
      this.artists.reverse();
      this.tracks = data[1];
      this.tracks.reverse();
      this.albums = data[2];
      this.albums.reverse();
      this.playlists = data[3];
      this.playlists.reverse();  
    })
    .catch(error => console.log('error: '+JSON.stringify(error)));
  }

  goArtist(artist: any) {
    this.navCtrl.push('ArtistPage', { artist: artist });
  }
  playTrack(track:any,index:number){
    this.spotifyProvider.playTrack(track,this.track_number == index);
    this.track_number = index;
  }
  goAlbum(album: any) {
    this.navCtrl.push('AlbumPage', { album: album });
  }
  goPlaylist(playlist: any) {
    this.navCtrl.push('PlayListPage', { playlist: playlist });
  }


}
