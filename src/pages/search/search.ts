import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  artists: Array<any> = [];
  albums: Array<any> = [];
  playlists: Array<any> = [];
  tracks: Array<any> = [];

  next = {
    artist: null,
    track: null,
    playlist: null,
    album: null
  };
  prev = {
    artist: null,
    track: null,
    playlist: null,
    album: null
  }

  anon = "https://image.freepik.com/iconos-gratis/la-imagen-del-usuario-con-el-fondo-negro_318-34564.jpg";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public spotifyProvider: SpotifyProvider,
    public platform: Platform) {
    
  }

  search(ev: any) {
    //buscar lo que sea
    this.spotifyProvider.getToken();
    if (ev.target.value != null) {
      let val = ev.target.value;
      this.spotifyProvider.searchObs(val).subscribe(
        (response: any) => {
          this.tracks = response.tracks.items;
          this.artists = response.artists.items;
          this.playlists = response.playlists.items;
          this.albums = response.albums.items;

          this.next.album = response.albums.next;
          this.next.artist = response.artists.next;
          this.next.playlist = response.playlists.next;
          this.next.track = response.tracks.next;
        }, error => {
          console.log(error);
        }
      );
    }

  }

  nextPage(index: number) {
    switch (index) {
      case 1: {
        if (this.next.artist != null) {
          this.spotifyProvider.nextPage(this.next.artist).subscribe(
            (response: any) => {
              this.artists = response.artists.items;
              this.next.artist = response.artists.next;
              this.prev.artist = response.artists.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      } case 2: {
        if (this.next.track != null) {
          this.spotifyProvider.nextPage(this.next.track).subscribe(
            (response: any) => {
              this.tracks = response.tracks.items;
              this.next.track = response.tracks.next;
              this.prev.track = response.tracks.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
      case 3: {
        if (this.next.album != null) {
          this.spotifyProvider.nextPage(this.next.album).subscribe(
            (response: any) => {
              this.albums = response.albums.items;
              this.next.album = response.albums.next;
              this.prev.album = response.albums.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
      case 4: {
        if (this.next.playlist != null) {
          this.spotifyProvider.nextPage(this.next.playlist).subscribe(
            (response: any) => {
              this.playlists = response.playlists.items;
              this.next.playlist = response.playlists.next;
              this.prev.playlist = response.playlists.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
    }
  }

  prevPage(index: number) {
    switch (index) {
      case 1: {
        if (this.prev.artist != null) {
          this.spotifyProvider.nextPage(this.prev.artist).subscribe(
            (response: any) => {
              this.artists = response.artists.items;
              this.next.artist = response.artists.next;
              this.prev.artist = response.artists.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      } case 2: {
        if (this.prev.track != null) {
          this.spotifyProvider.nextPage(this.prev.track).subscribe(
            (response: any) => {
              this.tracks = response.tracks.items;
              this.next.track = response.tracks.next;
              this.prev.track = response.tracks.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
      case 3: {
        if (this.prev.album != null) {
          this.spotifyProvider.nextPage(this.prev.album).subscribe(
            (response: any) => {
              this.albums = response.albums.items;
              this.next.album = response.albums.next;
              this.prev.album = response.albums.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
      case 4: {
        if (this.prev.playlist != null) {
          this.spotifyProvider.nextPage(this.prev.playlist).subscribe(
            (response: any) => {
              this.playlists = response.playlists.items;
              this.next.playlist = response.playlists.next;
              this.prev.playlist = response.playlists.previous;
            },
            error => {
              console.log(<any>error);
            }
          );
        }
        break;
      }
    }
  }

  goArtist(artist: any) {
    this.navCtrl.push('ArtistPage', { artist: artist });
  }
  goTrack(track: any) {
    this.navCtrl.push('TrackPage', { track: track });
  }
  goAlbum(album: any) {
    this.navCtrl.push('AlbumPage', { album: album });
  }
  goPlaylist(playlist: any) {
    this.navCtrl.push('PlayListPage', { playlist: playlist });
  }
}
