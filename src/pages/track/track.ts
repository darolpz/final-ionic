import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpotifyProvider } from '../../providers/spotify/spotify';
import { HistoryProvider } from '../../providers/history/history';
/* declare var test; */

/**
 * Generated class for the TrackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-track',
  templateUrl: 'track.html',
})
export class TrackPage {

  track: any;
  flag: boolean = false;
  title:string;
  firtTimeFlag = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private spotifyProvider: SpotifyProvider) {
    this.track = navParams.get('track');
   
  }

  ionViewDidEnter() {
    this.spotifyProvider.getTrack(this.track.id).subscribe(
      response => {
        this.track = response;
        this.title = this.track.name;      
        this.spotifyProvider.storeTrack(this.track);
    },error => {
      console.log(<any>error);
    });
  }

  playTrack(){
    this.spotifyProvider.playTrack(this.track, this.firtTimeFlag);
    this.flag = !this.flag;
    this.firtTimeFlag = true;
  }


}
